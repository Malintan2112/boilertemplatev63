import React, { useState, useCallback } from 'react'
import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
// STYLING USING
import * as Styles from 'App/Styles';
// REDUX USING
import AppActions from 'App/Redux/Actions';
import { useDispatch, } from 'react-redux';
import { useSelector } from 'react-redux';
// Components
import Button from 'App/Components/Widget/Button';
import { selectImage } from 'App/Helpers/ImageLoader';
import * as Hooks from 'App/Helpers/Hooks';
import ImagePart from 'App/Components/Widget/ImagePart';

const ButtonMemorilized = React.memo(Button);

export default function Home(props) {
    const dispatch = useDispatch();
    const language = useSelector((state) => state.language);

    // STATE
    const [urlImage, setUrlImage] = useState('')
    const [styleGlobalBtn] = useState({
        width: "100%",
        height: 50,
        backgroundColor: Styles.Colors.primary,
        style: { marginVertical: 10 }
    })

    // Function  With Memorilized
    const changeLanguage = () => {
        dispatch(AppActions.setLanguage("en"));
    }
    const changeLanguageMemorilized = useCallback(changeLanguage, []);

    const toDetail = () => {
        props.navigation.navigate('DetailTask')
    }
    const toDetailMemorilized = useCallback(toDetail, []);

    const useCamera = () => {
        selectImage('custom', false, {}, 'picker',
            {
                includeBase64: false,
                compressImageQuality: 0.1,
                mediaType: 'photo',
                forceJpg: true,
            },
            true).then((result) => {
                Hooks.consoleLog('Image Camera', result.path)
                setUrlImage(result.path);
            }).catch((err) => {
                Hooks.consoleError(`UploadImage Error`, err.message);
            });
    }
    const useScanBarcode = () => {
        props.navigation.navigate('ScanBarcode')
    }
    const useCameraMemorilized = useCallback(useCamera, []);
    const useScanBarcodeMemorilized = useCallback(useScanBarcode, [])

    return (
        <ScrollView>
            <Text>Home</Text>
            <ButtonMemorilized
                {...styleGlobalBtn}
                onPress={toDetailMemorilized}
                title="To Detail" />
            <Text>{language.title.account}</Text>
            <ButtonMemorilized
                {...styleGlobalBtn}
                onPress={changeLanguageMemorilized}
                title="Ganti Bahasa" />
            <Text>{"Camera"}</Text>
            <ImagePart
                source={urlImage}
                width={'100%'}
                height={300}
                style={{ marginVertical: 10 }}
                resizeMode={'cover'} />
            <ButtonMemorilized
                {...styleGlobalBtn}
                onPress={useCameraMemorilized}
                title="Camera" />

            <ButtonMemorilized
                {...styleGlobalBtn}
                onPress={useScanBarcodeMemorilized}
                title="Scan Barcode" />
        </ScrollView>
    )
}
