import React, { useCallback, useState } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'

// STYLING USING
import * as Styles from 'App/Styles';

export default function Profile() {

    const [count, setCount] = useState(1);
    const [childProps, setChildProps] = useState({ title: 'Hello' })

    const addCount = () => {
        setCount(curret => curret + 1)
    }
    const memolizeChildAction = useCallback(addCount, []);
    console.log('Parrent Component');
    return (
        <View>

            <View style={[Styles.Helpers.fullWidth, Styles.Helpers.center, { height: 50, backgroundColor: Styles.Colors.trueWhite }]}>
                <Text>{count}</Text>
            </View>
            <View style={[Styles.Helpers.fullWidth, Styles.Helpers.center, { backgroundColor: "pink", height: 100 }]}>
                <Text>{'Parent'}</Text>
            </View>
            <MemolizeChildComponent {...childProps} action={memolizeChildAction} />
            <TouchableOpacity
                onPress={addCount}
            >
                <View style={[Styles.Helpers.fullWidth, Styles.Helpers.center, { backgroundColor: Styles.Colors.trueWhite, height: 50, marginTop: 30 }]}>
                    <Text>{'Action'}</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const MemolizeChildComponent = React.memo(Child);
function Child(props) {
    sleep(2000)
    console.log('Child Component');
    return (
        <>
            <View style={[Styles.Helpers.fullWidth, Styles.Helpers.center, { backgroundColor: Styles.Colors.primary, height: 100 }]}>
                <Text>{'Child'}</Text>
                <Text>{props.title}</Text>
            </View>
            <TouchableOpacity
                onPress={props.action}
            >
                <View style={[Styles.Helpers.fullWidth, Styles.Helpers.center, { backgroundColor: Styles.Colors.trueWhite, height: 50, marginTop: 30 }]}>
                    <Text>{'Action Child'}</Text>
                </View>
            </TouchableOpacity>
        </>
    )
}
function sleep(milisecconds) {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now()
    } while (currentDate - date < milisecconds) {

    }
}