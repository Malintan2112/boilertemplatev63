import React, { useEffect } from 'react'
import { Image, StatusBar, Text, View } from 'react-native'
import { fcmService } from 'App/Helpers/FCMService';
import { localNotificationService } from 'App/Helpers/LocalNotificationService';
import * as Hooks from 'App/Helpers/Hooks';
import * as Styles from 'App/Styles';
import { selectImage } from 'App/Helpers/ImageLoader';
import { imageLoader } from '../Helpers/ImageLoader';


const TAG = "Splash Page";



const Splash = () => {

    useEffect(() => {
        initFCM()
    }, [])

    const initFCM = () => {
        return new Promise(resolve => {
            fcmService.registerAppWithFCM();
            fcmService.register(onRegister, onNotification, onOpenNotification);
            localNotificationService.configure(onOpenNotification);
            resolve(true);
            function onRegister(token) {
                Hooks.consoleLog(TAG + "FCM onRegister: ", token);
            }
            function onNotification(notify) {
                Hooks.consoleLog(TAG + "FCM onNotification: ", notify);
                let notify1 = notify.notification;
                const options = {
                    soundName: 'default',
                    playSound: true,
                    largeIcon: 'ic_launcher', // add icon large for Android (Link: app/src/main/mipmap)
                    smallIcon: 'ic_launcher' // add icon small for Android (Link: app/src/main/mipmap)
                }
                localNotificationService.showNotification(
                    0,
                    notify1.title,
                    notify1.body,
                    notify1,
                    options
                )
            }
            function onOpenNotification(notify) {
                Hooks.consoleLog(TAG + "FCM onOpenNotification: ", notify);
            }
        });
    }

    return (
        <View style={[Styles.Helpers.fullSize, Styles.Helpers.center, { backgroundColor: Styles.Colors.trueWhite }]}>
            <StatusBar translucent backgroundColor="transparent" />
            <Image source={Styles.Images.logo} style={{ width: 100, height: 100 }} />
        </View>
    )

}


export default Splash;