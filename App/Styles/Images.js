/**
 * Images should be stored in the `App/Assets/Images` directory and referenced using variables defined here.
 */

const IMAGES_PATH = 'App/Assets/Images/';
const ICONS_PATH = 'App/Assets/Icons/';

export default {





  // FOOTER ICONS
  footer_icon_home: require(ICONS_PATH + '/footer/home.png'),
  footer_icon_news: require(ICONS_PATH + '/footer/news.png'),
  footer_icon_profile: require(ICONS_PATH + '/footer/profile.png'),
  //

  // IMAGE 
  no_image: require(IMAGES_PATH + 'no_image.png'),
  logo: require(IMAGES_PATH + 'logo.png'),

}
