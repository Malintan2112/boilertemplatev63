import React, { useEffect, useMemo, useState } from 'react';
// NAVIGATION SETTING
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import TabBar from './TabBar';
// SESSION SETTING
import * as Sessions from 'App/Storages/Sessions';
// REDUX SETTING
import AppActions from 'App/Redux/Actions';
import { useDispatch, } from 'react-redux';
// PAGE 
import Home from 'App/Pages/Home/Home';
import Informasi from 'App/Pages/Informasi/Informasi';
import Profile from 'App/Pages/Profile/Profile';
import DetailTask from 'App/Pages/Task/DetailTask';
import Splash from 'App/Pages/Splash';
import ScanBarcode from 'App/Components/Widget/ScanBarcode/ScanBarcode';







// STACK NAVIGATION
const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const MainStack = createStackNavigator();
// DRAWER NAVIGATION
const Drawer = createDrawerNavigator();
// BOTTOM TABS
const TabsScreen = () => {
    const getTabBarVisibility = (route) => {
        try {
            const routeName = route.state
                ? route.state.routes[route.state.index].name
                : '';
            console.log(route.state.index)

            if (route.state.index == 0) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            return true;
        }
    }
    return (
        <Tabs.Navigator tabBar={props => <TabBar  {...props} />}>
            <Tabs.Screen
                name="Home"
                component={Home}
            />
            <Tabs.Screen
                name="Informasi"
                component={Informasi} />
            <Tabs.Screen
                name="Profile"
                component={Profile} />

        </Tabs.Navigator>
    )
}
// MAIN STACK
const MainStackScreen = () => (
    <MainStack.Navigator screenOptions={{ headerShown: false }}>
        <MainStack.Screen name="Tabs" component={TabsScreen} />
        <MainStack.Screen name="DetailTask" component={DetailTask} />
        <MainStack.Screen name="ScanBarcode" component={ScanBarcode} />
    </MainStack.Navigator>
)
// NAVIGATON OPTION
const NavigationApps = () => {
    const [isLoading, setIsLoading] = useState(true);
    const dispatch = useDispatch();
    const componentLoadSession = () => {
        Sessions.prepare().then(dataSession => {
            dispatch(AppActions.setLanguage(dataSession.LANGUAGE ? dataSession.LANGUAGE : 'id'))
        }).catch(err => {
        });
    }
    useEffect(() => {
        componentLoadSession()
        setTimeout(() => {
            setIsLoading(false)
        }, 2000)
    }, [])
    if (isLoading) {
        return <Splash />
    }
    return (
        <NavigationContainer>
            {MainStackScreen()}
        </NavigationContainer>
    )
}

export default NavigationApps;