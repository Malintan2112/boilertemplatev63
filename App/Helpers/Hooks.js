



export function consoleLog(TAG: String, message: String, force: Boolean = false) {
    if (__DEV__ || force === true) { // if in Development mode
        console.log(`%c${TAG} =>`, 'color: rgb(61, 217, 53);', message);
    }
}
export function consoleWarn(TAG: String, message: String, force: Boolean = false) {
    if (__DEV__ || force === true) { // if in Development mode
        console.log(`%c${TAG} =>`, 'background: rgb(255, 174, 36); color: rgb(255, 255, 255);', message);
    }
}
export function consoleError(TAG: String, message: String, force: Boolean = false) {
    if (__DEV__ || force === true) { // if in Development mode
        console.log(`%c${TAG} =>`, 'background: rgb(246, 60, 60); color: rgb(255, 255, 255);', message);
    }
}
