import {
  Alert,
} from 'react-native';

const baseURL = "";

export const createRequestTimeOut = (params, timeout = 100) => {
  return new Promise(async (resolve, reject) => {
    try {
      await fetch(baseURL + params.link, {
        method: params.method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer' + params.token
        },
        body: JSON.stringify(params.data)
      })
        .then((response) => response.json())
        .then((responseData) => {
          // console.log(
          //   params.method + " Response",
          //   "Response Body -> " + JSON.stringify(responseData)
          // )
          // const response = JSON.stringify(responseData);
          resolve(responseData);

        }).catch((error) => {
          reject(error)
        })
        .done();
      setTimeout(() => {
        reject({ status: false, message: "request time out" });
      }, timeout);
    } catch (error) {
      reject(error)
    }
  })
}




export const createRequestGlobalTimeOut = (params, timeout = 100) => {
  return new Promise(async (resolve, reject) => {
    try {
      await fetch(params.link, {
        method: params.method,
        headers: params.header,
        body: JSON.stringify(params.data)
      })
        .then((response) => response.json())
        .then((responseData) => {
          resolve(responseData);
        }).catch((error) => {
          reject(error)
        })
        .done();
      setTimeout(() => {
        reject({ status: false, message: "request time out" });
      }, timeout);
    } catch (error) {
      reject(error)
    }
  })
}

